class test {

  package { "apache2":
    ensure => absent
  }

  package { "nginx":
    ensure => present
  }
  
  file { "/var/www/html/index.html":
    source => "puppet:///modules/test/index.html",
    ensure => present
  }

  service { 'nginx':
    ensure => 'running',
    enable => 'true',
  }
} 
